package service

import (
	"errors"
	"go-shop-demo/app/dao"
	"go-shop-demo/app/dto"
	"go-shop-demo/app/model"
)

func AddOrde(ord dto.OrderDto) (uint, error) {
	//存储订单数据
	total := float64(0)
	for _, res := range ord.OrderGoods {
		total = res.Price*float64(res.GoodNum) + total
	}
	order := model.Order{UserName: ord.UserName, Price: total, Status: 0}
	orderid, err := dao.AddOrd(order, ord.OrderGoods)
	if err != nil {
		return 0, errors.New("订单插入失败")
	}
	return orderid, nil

}

func DeleteOrder(id uint) {
	dao.DeleteOrderGoodsByOrderId(id)
	dao.DeleteOrder(id)
	return
}

func Detail(detail dto.DetailOrderDto) (result []model.RespOder, err error) {
	result, err = dao.DetailOrd(detail)
	if err != nil {
		err = errors.New("查询订单失败")
		return result, err
	}
	return result, err
}

func Pay(ids []uint) error {
	for _, id := range ids {
		err := dao.CheckOrder(id)
		if err != nil {
			return err
		}
		//更新支付状态
		result := dao.UpdatePay(id)
		if !result {
			return errors.New("更新状态失败")
		}
	}
	return nil
}
