package service

import (
	"errors"
	"go-shop-demo/app/dao"
	"go-shop-demo/app/model"
)

func AddGoods(goods model.Goods) error {
	//查询数据库货物名是否存在
	res := dao.CheckGoods(goods.GoodsName)
	if res {
		return errors.New("货物已经存在")
	}
	//创建货物
	err := dao.AddGoods(goods)
	return err
}

func DeleteGoods(id uint) {
	dao.DeleteGoods(id)
	return
}

func GetGoods(page, size int) (total int64, items []model.Goods) {
	total, items = dao.GetGoods(page, size)
	return
}

func GetSingleGoods(goodsname string) (goods model.Goods) {
	goods = dao.GetSingleGoods(goodsname)
	return goods
}
