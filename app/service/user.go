package service

import (
	"errors"
	"go-shop-demo/app/dao"
	"go-shop-demo/app/model"
)

func Register(name, password string) error {
	//查询数据库用户名是否存在
	exist := dao.CheckUser(name)
	if exist {
		return errors.New("用户已存在")
	}
	//注册用户
	err := dao.Regist(name, password)
	return err
}

func Login(name, password string) (err error, user model.User) {
	user = dao.GetUser(name)
	if user.ID == 0 {
		return errors.New("用户不存在"), user
	}
	if user.Password != password {
		return errors.New("密码错误"), user
	}
	return
}
