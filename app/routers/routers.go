package routers

import (
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"go-shop-demo/app/common"
	"go-shop-demo/app/controller"
	"net/http"
)

// SetupRouter 配置路由信息
func SetupRouter() *gin.Engine {
	//设置模式
	gin.SetMode(gin.DebugMode)
	r := gin.Default()
	//跨域中件件
	r.Use(cors.Default())
	r.Use(common.Recover)
	r.LoadHTMLFiles("./app/templates/index.html")
	r.Static("/static", "./app/static")
	r.GET("/", func(c *gin.Context) {
		c.HTML(http.StatusOK, "index.html", nil)
	})
	//用户注册
	userGroup := r.Group("/user")
	{
		userGroup.POST("/register", controller.Register)
		//登录
		userGroup.POST("/login", controller.Login)
	}
	//商品相关
	goodsGroup := r.Group("goods")
	{
		goodsGroup.POST("/add", controller.AddGoodser)
		//商品列表
		goodsGroup.GET("/list", controller.GetGoods)
		//商品删除
		goodsGroup.DELETE("/delete", controller.DeleteGoods)
		//单个商品信息
		goodsGroup.GET("/single", controller.GetSingleGoods)
	}
	//订单相关
	orderGroup := r.Group("/order")
	{
		//订单创建
		orderGroup.POST("/add", controller.AddOrder)
		//订单删除
		orderGroup.DELETE("/delete", controller.DeleteOrder)
		//订单详情
		orderGroup.GET("/detail", controller.DetailOrder)
		//订单支付
		orderGroup.PUT("/pay", controller.Payer)
	}
	return r
}
