package common

type Pageable struct {
	Page  int   `json:"page" form:"page"`
	Size  int   `json:"size" form:"size"`
	Total int64 `json:"total"`
}

func NewPageable(page, size int, total int64) Pageable {
	return Pageable{Page: page, Size: size, Total: total}
}
