package common

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

func NewResponse(code int, msg string, data interface{}) gin.H {
	return gin.H{"code": code, "msg": msg, "data": data}
}

func GetNilResponse() gin.H {
	return gin.H{"code": http.StatusOK, "msg": "success", "data": nil}
}

func GetDataResponse(data interface{}) gin.H {
	return gin.H{"code": http.StatusOK, "msg": "success", "data": data}
}

func GetResponseWithMsg(data interface{}, msg string) gin.H {
	return gin.H{"code": http.StatusOK, "msg": msg, "data": data}
}

type Result struct {
	Content  interface{} `json:"content"`
	Pageable `json:"pageable"`
}

func GetPageResult(data interface{}, page, size int, total int64) interface{} {
	return Result{Content: data, Pageable: NewPageable(page, size, total)}
}
