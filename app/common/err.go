package common

type Error struct {
	Code int
	Msg  string
}

func CheckErr(err error, code int, msg string) {
	if err != nil {
		if msg == "" {
			msg = err.Error()
		}
		e := Error{Code: code, Msg: msg}
		panic(e)
	}
}
