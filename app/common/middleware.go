package common

import (
	"github.com/gin-gonic/gin"
	"go-shop-demo/app/consts"
	"log"
)

func Recover(ctx *gin.Context) {
	defer func() {
		err := recover()
		PostProcessTx(err)
		if err != nil {
			log.Print(err)
			if e, ok := err.(Error); ok {
				ctx.JSON(e.Code, NewResponse(e.Code, e.Msg, nil))
			} else {
				if e, ok := err.(error); ok {
					ctx.JSON(consts.ServerError, NewResponse(consts.ServerError, e.Error(), nil))
				} else {
					ctx.JSON(consts.ServerError, NewResponse(consts.ServerError, "err", nil))
				}
			}
		}
	}()
	ctx.Next()
}
