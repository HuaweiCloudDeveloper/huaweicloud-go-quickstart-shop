package common

import (
	"embed"
	"gopkg.in/yaml.v3"
)

type config struct {
	Port string `yaml:"port" default:"8080" env:"port"`
	Db   struct {
		Name     string `yaml:"name" env:"name"`
		Password string `yaml:"password" env:"password"`
		Host     string `yaml:"host" env:"host"`
		Port     string `yaml:"port" env:"host"`
		Database string `yaml:"database" env:"database"`
	}
}

var Config = config{}

var FS embed.FS

func InitConfig() {
	file, err := FS.ReadFile("application.yml")
	CheckErr(err, 500, "")
	err = yaml.Unmarshal(file, &Config)
	CheckErr(err, 500, "")
}
