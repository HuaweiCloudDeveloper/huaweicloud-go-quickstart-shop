package model

type OrderGoods struct {
	ID        uint   `gorm:"primaryKey"`
	OrderId   uint   `gorm:"type:int(11);not null"`
	GoodsName string `gorm:"type:varchar(20);not null"`
	GoodNum   uint   `gorm:"type:int(11)"`
}

func (o OrderGoods) TableName() string {
	return "order_goods"
}

type Order struct {
	ID       uint    `gorm:"primaryKey"`
	UserName string  `gorm:"type:varchar(20);not null"`
	Price    float64 `gorm:"type:float"`
	Status   int     `gorm:"type:int"` // 0 待支付 1 已支付
}

func (o Order) TableName() string {
	return "order"
}

type RespOder struct {
	ResOrder      Order
	ResOrderGoods OrderGoods
}
