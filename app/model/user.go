package model

type User struct {
	ID       uint   `gorm:"primaryKey"`
	Name     string `gorm:"type:varchar(20);not null;unique"`
	Password string `gorm:"type:varchar(255);not null"`
}

func (u User) TableName() string {
	return "user"
}
