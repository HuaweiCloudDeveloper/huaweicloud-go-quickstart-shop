package model

type Goods struct {
	ID        uint    `gorm:"primaryKey"`
	GoodsName string  `gorm:"type:varchar(20);not null;unique"`
	Price     float64 `gorm:"type:float;not null"`
}

func (g Goods) TableName() string {
	return "goods"
}
