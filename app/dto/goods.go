package dto

import "go-shop-demo/app/model"

type AddGoodsDto struct {
	GoodsName string  `json:"goodsName"`
	Price     float64 `json:"price"`
}

type SingleGoods struct {
	GoodsName string `form:"goodsname"`
}

type DeleteGoodsDto struct {
	ID uint `json:"id"`
}

type GoodsPageDto struct {
	ID        uint    `json:"id"`
	GoodsName string  `json:"goodsName"`
	Price     float64 `json:"price"`
}

func NewGoodsPageDto(goods []model.Goods) []GoodsPageDto {
	dtos := make([]GoodsPageDto, len(goods))
	for i, good := range goods {
		dto := GoodsPageDto{ID: good.ID, GoodsName: good.GoodsName, Price: good.Price}
		dtos[i] = dto
	}
	return dtos
}
