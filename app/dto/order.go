package dto

type OrderPayDto struct {
	IDS []uint `json:"ids"`
}

type OrderGoodsDto struct {
	GoodsName string  `json:"goodsName"`
	Price     float64 `json:"price"`
	GoodNum   uint    `json:"goodNum"`
}
type OrderDto struct {
	UserName   string          `json:"userName"`
	OrderGoods []OrderGoodsDto `json:"orderGoods"`
}

type DeleteOrderDto struct {
	OrderID uint `json:"orderId"`
}

type DetailOrderDto struct {
	UserName string `form:"username"`
	Status   int    `form:"status"`
}
