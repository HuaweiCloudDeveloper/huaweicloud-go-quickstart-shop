package dto

import "go-shop-demo/app/model"

type UserDto struct {
	Name     string `json:"name"`
	Password string `json:"password"`
}

type UserDetailDto struct {
	ID   uint   `json:"id"`
	Name string `json:"name"`
}

func NewUserDetailDto(user model.User) UserDetailDto {
	return UserDetailDto{ID: user.ID, Name: user.Name}
}
