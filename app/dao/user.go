package dao

import (
	"errors"
	"go-shop-demo/app/common"
	"go-shop-demo/app/consts"
	"go-shop-demo/app/model"
)

//查询用户名是否存在
func CheckUser(name string) (exist bool) {
	db := common.GetDB()
	user := model.User{}
	err := db.Where("name = ?", name).Find(&user).Error
	common.CheckErr(err, 500, "")
	if user.ID != 0 {
		return true
	}
	return false
}

func Regist(name string, password string) error {
	tx := common.GetTx()
	user := model.User{Name: name, Password: password}
	err := tx.Save(&user).Error
	if err != nil {
		return errors.New("插入数据失败")
	}
	tx.Commit()
	return nil
}

func GetUser(name string) (user model.User) {
	db := common.GetDB()
	err := db.Where("name = ?", name).Find(&user).Error
	common.CheckErr(err, consts.ServerError, "")
	return
}
