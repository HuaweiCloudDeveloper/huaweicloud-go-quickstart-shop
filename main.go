package main

import (
	"embed"
	"go-shop-demo/app/common"
	"go-shop-demo/app/routers"
)

//go:embed application.yml
var fs embed.FS

func main() {
	common.FS = fs
	common.InitConfig()
	//初始化数据库
	common.InitDB()
	//加载路由
	r := routers.SetupRouter()
	r.Run(":" + common.Config.Port)
}
