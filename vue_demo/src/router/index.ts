const router = [
  {
      path: '/',
      name: '/',
      component: () => import('@/views/Login/login.vue'),
  },
  {
      path: '/login',
      name: '/login',
      component: () => import('@/views/Login/login.vue'),
  },
  {
    path: '/register',
    name: '/register',
    component: () => import('@/views/register/register.vue'), 
},
  {
     path: '/home',
     name: '/home',
     component: () => import('@/views/Home/Home.vue'),
     children: [
          {
            path: '/home/service-list/service-details/:goodsname',
            name: '/home/service-list/service-details',
            component: () => import('@/views/ServiceList/ServiceDetails.vue')
          },
          {
            path: '/home/service-list',
            name: '/home/service-list',
            component: () => import('@/views/ServiceList/ServiceList.vue')
          },
          {
            path: '/home/my-orders',
            name: '/home/my-orders',
            component: () => import('@/views/MyOrder/MyOrder.vue'),
          },
          {
            path: '/home/order-history',
            name: '/home/order-history',
            component: () => import('@/views/orderHistory/OrderHistory.vue'),
          },
     ]
  }
];

export default router;
