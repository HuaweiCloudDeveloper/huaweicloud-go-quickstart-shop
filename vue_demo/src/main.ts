import { createApp} from 'vue'
import { createRouter,createWebHashHistory } from 'vue-router'
import App from './App.vue'
import selfRoutes from './router'
import store from './store'
import ElementPlus from 'element-plus';
import zhCn from 'element-plus/es/locale/lang/zh-cn';
import 'element-plus/dist/index.css';
import '../src/assets/style/default.css'
import qs from 'qs'


const router = createRouter({
    history: createWebHashHistory(),
    routes: selfRoutes,
})

const app = createApp(App)
app.use(ElementPlus, {
    locale: zhCn,
});

app.use(store)
app.use(router)
app.use(ElementPlus)
app.mount('#app')
