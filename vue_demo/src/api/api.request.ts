import HttpRequest from './axios';

const baseUrl = 'http://localhost:8080';
const baseLogUrl = 'http://100.85.219.35:30726/';
const axios = new HttpRequest(baseUrl);
export { axios, baseLogUrl };
