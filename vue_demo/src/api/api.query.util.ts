import { AxiosPromise } from 'axios';
import { axios } from './api.request';
import qs from 'qs';

interface Response {
    code: number;
    result: any;
    message: string;
}

export function queryFn1(url: string, params: any): Promise<Response> {
    return axios.request({
        url,
        method: params.method,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        data: qs.stringify(params.data),
    });
}

export function queryFn(url: string, params: any): Promise<Response> {
    return axios.request({
        url,
        method: params.method,
        data: params.data,
    });
}

/**
 * 登录接口
 * @param params
 */
export function userLogin(params: { method: string; data: {} }) {
    return queryFn('/saas-user-info/login/user-login', params);
}

/**
 * 登录接口
 * @param params
 */
 export function getUsername(params: { method: string; data: {} }) {
    return queryFn1('user/login', params);
}

/**
 * 注册接口
 * @param params
 */
 export function getRegister(params: { method: string; data: {} }) {
    return queryFn1('user/register', params);
}

/**
 * 登录接口
 * @param params
 */
 export function getGoodList(params: { method: string; data: {} }) {
    return queryFn('goods/list', params);
}

/**
 * 登录接口
 * @param params
 */
 export function getGoodSingle(params: { method: string; data: { goodsname: string } }) {
    return queryFn(`goods/single?goodsname=${params.data.goodsname}`, params);
}

/**
 * 创建订单接口
 * @param params
 */
 export function getOrderAdd(params: { method: string; data: {} }) {
    return queryFn('order/add', params);
}

/**
 * 整个订单接口
 * @param params
 */
 export function getOrderDetail(params: { method: string; data: { username: string; status: number } }) {
    return queryFn(`order/detail?username=${params.data.username}&status=${params.data.status}`, params);
}

/**
 * 立即支付接口
 * @param params
 */
 export function getOrderPay(params: { method: string; data: { username: string } }) {
    return queryFn('order/pay', params);
}

/**
 * 订单删除接口
 * @param params
 */
 export function getOrderDelete(params: { method: string; data: {} }) {
    return queryFn('order/delete', params);
}

/**
 * 单个服务查询详情接口
 * @param params
 * @serviceId 服务id
 */
export function getServiceById(params: {
    method: string;
    data: {
        serviceId: string;
    };
}) {
    return queryFn(`/saas-public/housekeeper/getServiceById?serviceId=${params.data.serviceId}`, params);
}

/**
 * 查询我的订单列表
 * @param { pageIndex，pageSize，status } params
 */
export function getOrdersList(params: { method: string; data: {} }) {
    return queryFn('/saas-order/order/customer-orders', params);
}

/**
 * 查询订单详情接口
 * @param params
 * @orderId 订单id
 */
export function getOrderDetails(params: {
    method: string;
    data: {
        orderId: string;
    };
}) {
    return queryFn(`/saas-order/order/order/orderId/${params.data.orderId}`, params);
}

/**
 * 用户创建订单 POST
 * @param params
 */
export function createOrder(params: { method: string; data: {} }) {
    return queryFn('/saas-order/order/order', params);
}

/**
 * 用户注册 POST
 * @param params
 */
export function registerUser(params: { method: string; data: {} }) {
    return queryFn('/saas-user-info/user/info', params);
}

export function getAllMsg(params: any) {
    return queryFn('/saas-message/msg/all-msg', params);
}

/**
 * 获取已读/未读消息 GET
 * @param params
 */
export function getMsg(params: any) {
    return queryFn(`/saas-message/msg/msg/${params.data.status}`, params);
}

/**
 * 获取未读消息数量 GET
 * @param params
 */
export function getCount(params: any) {
    return queryFn('/saas-message/msg/count', params);
}

/**
 * 将消息标为已读 POST
 * @param params
 */
export function setMsg(params: any) {
    return queryFn('/saas-message/msg/state', params);
}

/**
 * 删除已读消息 POST
 * @param params
 */
export function deleteMsg(params: any) {
    return queryFn('/saas-message/msg/del', params);
}
