function getCookie(name: string): string {
    let arr: RegExpMatchArray;
    const reg = new RegExp(`(^| )${name}=([^;]*)(;|$)`);
    const temp = document.cookie.match(reg);
    if (temp !== null && temp !== undefined) {
        arr = temp;
        return decodeURIComponent(arr[2]);
    }
    return '';
}

/**
 * 存cookie
 */
function setCookie(name: string, value: string, expiresDate = 24) {
    const date = new Date(); // 获取当前时间
    date.setTime(date.getTime() + expiresDate * 3600 * 1000); // 格式化为cookie识别的时间
    document.cookie = `${name}=${value};expires=${date.toUTCString()}`;
}

/**
 * 删除cookie
 */
function deleteCookie(name: string) {
    setCookie(name, '', -1);
}

let timeout: any = null;
function debounce(fn: any, wait: any) {
    if (timeout !== null) clearTimeout(timeout);
    timeout = setTimeout(fn, wait);
}


export { setCookie, getCookie, deleteCookie, debounce };