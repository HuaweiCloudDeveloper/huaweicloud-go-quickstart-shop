#!/bin/bash

PROJECT_ROOT=$(cd `dirname $0/`/..;pwd)

cd $PROJECT_ROOT

export GO111MODULE=on
export GONOSUMDB=*

go mod tidy
CGO_CFLAGS="-fstack-protector-strong -D_FORTIFY_SOURCE=2 -O2" go build -buildmode=pie --ldflags "-s -linkmode 'external' -extldflags '-Wl,-z,now'" main.go

tar -czvf $PROJECT_ROOT/huaweicloud-go-quickstart-shop-${CID_BUILD_TIME}.tar.gz . --exclude=.cid --exclude=.codecheck
