FROM golang:1.16 as build-stage
WORKDIR /go-shop-demo
COPY . .
RUN go env -w CGO_ENABLED=0 GOOS=linux GOARCH=amd64
RUN go env -w GOPROXY=https://goproxy.cn,direct
RUN go mod tidy
RUN go build

FROM alpine as production-stage
ENV TZ=Asia/Shanghai
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
COPY --from=build-stage /go-shop-demo /go-shop-demo
WORKDIR /go-shop-demo
ENTRYPOINT ["./go-shop-demo"]