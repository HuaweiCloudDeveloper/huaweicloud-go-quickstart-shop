module go-shop-demo

go 1.16

require (
	github.com/gin-contrib/cors v1.4.0
	github.com/gin-gonic/gin v1.8.1
	github.com/google/go-cmp v0.5.6 // indirect
	github.com/jinzhu/gorm v1.9.16
	golang.org/x/crypto v0.0.0-20210817164053-32db794688a5 // indirect
	golang.org/x/sys v0.0.0-20211124211545-fe61309f8881 // indirect
	golang.org/x/xerrors v0.0.0-20200804184101-5ec99f83aff1 // indirect
	gopkg.in/yaml.v3 v3.0.1
	gorm.io/driver/mysql v1.3.6
	gorm.io/gorm v1.23.8
)
